//
//  File.swift
//  PianoAnton
//
//  Created by Anton on 19.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Foundation



func readCellsFromFile() -> [String] {
//    let path = Bundle.main.path(forResource: "test", ofType: "txt")
let path = (NSTemporaryDirectory() + "test.txt") as String
    let file: FileHandle? = FileHandle(forReadingAtPath: path)
    var tempStr = String()
    if file != nil {
        let data = file?.readDataToEndOfFile()
        file?.closeFile()
        tempStr = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
    }
    else {
        print("Ooops! Something went wrong!")
    }
    let tempArray: [String] = tempStr.components(separatedBy: "\n")
    return tempArray
}

func ifNotExist() {
    let fileManager = FileManager.default
    let path = (NSTemporaryDirectory() + "test.txt") as String
    // Check if file exists, given its path
    
    if fileManager.fileExists(atPath: path) {
        print("File exists")
    } else {
        let data = ("Silentium est aureum" as NSString).data(using: String.Encoding.utf8.rawValue)
        
        if !fileManager.createFile(atPath: path, contents: data, attributes: nil){
            print("Impossible to create a file")
        }
    }
}
func writeCellsToFile(_ cells: [String]) {
//    let path = Bundle.main.path(forResource: "test", ofType: "txt")
    let path = (NSTemporaryDirectory() + "test.txt") as String
    var contents = String()
    var cell = String()
    for i in 0..<cells.count {
        cell = cells[i]
        if i == cells.count - 1 {
            contents.append(cell)
        } else {
            contents.append(cell + "\n")
        }
    }
    do {
        try contents.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
    }
    catch let error as NSError {
        print("Ooops! Something went wrong: \(error)")
    }
}
