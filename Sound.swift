//
//  Sound.swift
//  PianoAnton
//
//  Created by Anton on 18.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Foundation
import AVFoundation

class Sound {
    
    var audioURl: URL?
    var audioPlayer = AVAudioPlayer()
 
    init(withSoundURl audioUrl: URL) {
        self.audioURl = audioUrl
    }
    func playSound()  {
        self.audioPlayer = try! AVAudioPlayer(contentsOf: self.audioURl!)
        self.audioPlayer.prepareToPlay()
        self.audioPlayer.currentTime = 0
        self.audioPlayer.play()
    }
    
   
}

