//
//  ViewController.swift
//  PianoAnton
//
//  Created by Anton on 18.12.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var btnCollection: [UIButton]!
    @IBOutlet weak var logTextView: UITextView!
    
    
    let notes = ["do","re","mi","fa","sol","ly","si"]
    var audioSamplesArray:[Sound] = []
    
    func initLAPAudioWirhURL() -> Void {
        for i in 0..<notes.count {
            self.audioSamplesArray.append(Sound(withSoundURl: URL(fileURLWithPath: Bundle.main.path(forResource: self.notes[i], ofType: "mp3")!)))
        }
    }
    
    @IBAction func btnClick(_ sender: UIButton) {
        var content = readCellsFromFile()
        let date = Date()
        let calendar = Calendar.current
      let currentTime = calendar.dateComponents([.hour,.minute,.second], from: date)
        
        let resultTime = " " + String(describing: currentTime.hour!) + ":" + String(describing: currentTime.minute!) + ":" + String(describing: currentTime.second!)


        let logStr = (sender.titleLabel?.text)! + resultTime + "\t" + "\n"
        self.logTextView.text! = logStr + self.logTextView.text!
        content.insert(logStr, at: 0)
        writeCellsToFile(content)
        self.audioSamplesArray[sender.tag].playSound()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initLAPAudioWirhURL()
        ifNotExist()
        
     let arrLogFromFile = readCellsFromFile()
        
        for i in arrLogFromFile {
            self.logTextView.text! = i + self.logTextView.text!
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

